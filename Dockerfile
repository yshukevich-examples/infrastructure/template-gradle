FROM registry.gitlab.com/yshukevich-examples/infrastructure/template-java:1.0.64

USER root

ENV GRADLE_VERSION 8.10.2
ARG GRADLE_DOWNLOAD_SHA256=2ab88d6de2c23e6adae7363ae6e29cbdd2a709e992929b48b6530fd0c7133bd6
ARG BASE_URL=https://services.gradle.org/distributions/gradle
ENV GRADLE_HOME=/usr/share/gradle/gradle-${GRADLE_VERSION}
ENV PATH=$PATH:$GRADLE_HOME/bin

RUN mkdir -p /usr/share/gradle \
    && wget --no-verbose --output-document=gradle.zip "${BASE_URL}-${GRADLE_VERSION}-all.zip" \
    && echo "${GRADLE_DOWNLOAD_SHA256} *gradle.zip" | sha256sum -c - \
    && unzip gradle.zip -d /usr/share/gradle \
    && cd ${GRADLE_HOME} \
    && rm -f gradle.zip \
    && ln -s ${GRADLE_HOME}/bin /root/.gradle \
    && echo "Testing Gradle installation" \
    && gradle --version

USER java

CMD ["gradle"]
